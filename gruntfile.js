/* globals module, require */

'use strict';

module.exports = grunt => {
	require('jit-grunt')(grunt, {
		eslint: 'gruntify-eslint'
	});

	const pkg = grunt.file.readJSON('package.json');

	/**************************************************************************
	* CONFIG
	**************************************************************************/

	grunt.initConfig({
		pkg: pkg,

		bump: {
			options: {
				files: ['package.json'],
				commit: false,
				createTag: false,
				push: false,
				force: true
			}
		},

		eslint: {
			options: {
				config: './.eslintrc'
			},

			all: {
				src: [
					'./store.js',
					'demo/*.js'
				]
			}
		}
	});

	/**************************************************************************
	* TASKS
	**************************************************************************/

	grunt.registerTask('lint', [
		'eslint'
	]);

	/**************************************************************************
	* /END
	**************************************************************************/
};
