# Reduxion

A minimalist implementation of Redux. Not that there was much to take out. Redux is 2kB minified, Reduxion is 0.5kB. But I've removed features like middleware and combining reducers. So saying my library is four times smaller isn't fair.

I'm not making any negative statements about Redux with this library. I like Redux and use Redux. This library was written as a means to understand Redux methodology better. And to study implementing them in my own way. Plus, I have some side projects where vanilla Javascript is enforced, but I still want to use data management like Redux. This was born from that.

# Installation

It's a node module, so `npm install --save REPO_URL`.

# Running

You can run the demo from its folder `node demo/test.js`. I use ES6 code for the demo, so use a modern version of node to run the demo. The library code itself is written in ES5.
