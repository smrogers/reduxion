const ActionType = require('./action.type.js');

/*****************************************************************************/

function modify(state, newState) {
	return Object.assign({}, state, newState);
}

function adjustValueReducer({type, adjustment}, state) {
	return modify(state, {
		value: state.value + adjustment
	});
}

function resetValueReducer(action, state) {
	return modify(state, {
		value: 0
	});
}

/*****************************************************************************/

// I prefer the following setup than multiple top-level reducers functions.
// I can break a large function map into two or more indepedent objects in
// their own modules that I can combine in a final map without needing to
// extra reducer-"combining" logic.

const reducerMap = Object.freeze({
	[ActionType.AdjustValue]: adjustValueReducer,
	[ActionType.ResetValue]: resetValueReducer
});

/*****************************************************************************/

// And after putting in the effort to remove multiple switch statements from a
// large, theoretical application, below I've just recreated the switch
// functionality in javascript.

function mainReducer(action, state) {
	const reducer = reducerMap[action.type];
	if (reducer !== undefined) {
		return reducer(action, state);
	}

	return state;
}

/*****************************************************************************/

module.exports = mainReducer;
