const ActionType = require('./action.type.js');

/*****************************************************************************/

function adjustValue(adjustment = 1) {
	return Object.freeze({
		type: ActionType.AdjustValue,
		adjustment
	});
}

function resetValue() {
	return Object.freeze({
		type: ActionType.ResetValue
	});
}

/*****************************************************************************/

module.exports = Object.freeze({
	adjustValue,
	resetValue
});
