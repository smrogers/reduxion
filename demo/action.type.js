module.exports = Object.freeze({
	AdjustValue: 'adjust-value',
	ResetValue: 'reset-value'
});
