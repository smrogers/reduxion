/* eslint no-console: 0 */

const createStore = require('../store.js');
const mainReducer = require('./reducers.js');
const {adjustValue, resetValue} = require('./action.creators.js');

/*****************************************************************************/

const initialState = {value: 0};
const store = createStore(mainReducer, initialState);

const unsubscribe = store.subscribe(storeRef => {
	console.log('New state', storeRef.getState());
});

/*****************************************************************************/

store.dispatch(adjustValue(1));
store.dispatch(adjustValue(1));
store.dispatch(resetValue());

unsubscribe();

store.dispatch(adjustValue(10));
console.log('Final state', store.getState());
