/* eslint no-var: 0 */

function noop() {}

// array.filter without creating a new array
function filter(arr, callback) {
	arr = arr || [];
	callback = callback || noop;

	var numberToRemove = 0;

	for (var i = 0; i < arr.length; i++) {
		var hitFound = callback(arr[i], i, arr);
		if (hitFound) {
			numberToRemove += 1;
			continue;
		}

		if (numberToRemove < 1) { continue; }
		arr[i - 1] = arr[i];
	}

	for (var k = 0; k < numberToRemove; k++) {
		arr.pop();
	}
}

function createStore(mainReducer, initialState) {
	mainReducer = mainReducer || noop;

	var listeners = [];
	var state = initialState;

	var store = Object.freeze({
		getState: function getState() {
			return state;
		},
		dispatch: function dispatch(action) {
			state = mainReducer(action, state);
			listeners.forEach(function foreachListener(listener) {
				listener(store);
			});
		},
		subscribe: function subscribe(callback) {
			listeners.push(callback);
			return function unsubscribe() {
				filter(listeners, function filterListeners(listener) {
					return (listener === callback);
				});
			};
		}
	});

	return store;
}

/*****************************************************************************/

module.exports = createStore;
